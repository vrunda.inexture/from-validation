import React from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { validationSchema } from './validation/ValidationSchema';
import './App.css';

const App = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
    mode: 'onChange',
    init: { age: '' },
  });
  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <div className='mt-5'>
      <h1> React Hook Form</h1>
      <hr />
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className='form-group'>
          <label htmlFor='name'>Name </label>
          <input
            {...register('name')}
            type='text'
            className='form-control'
            placeholder='John Doe'
          />
          <p className='text-danger'>{errors.name?.message}</p>
        </div>
        <div className='form-group'>
          <label htmlFor='email'>Email </label>
          <input
            {...register('email')}
            type='text'
            className='form-control'
            placeholder='name@example.com'
          />
          <p className='text-danger'>{errors.email?.message}</p>
        </div>
        <div className='form-group'>
          <label htmlFor='password'>Password </label>
          <input
            {...register('password')}
            type='text'
            className='form-control'
            placeholder='Enter your password'
          />
          <p className='text-danger'>{errors.password?.message}</p>
        </div>
        <div className='form-group'>
          <label htmlFor='age'>Age</label>
          <input
            {...register('age')}
            type='text'
            className='form-control'
            placeholder='Enter your age'
          />
          <p className='text-danger'>{errors.age?.message}</p>
        </div>
        <div className='form-group'>
          <label htmlFor='phone'>Phone</label>
          <input
            {...register('phone')}
            type='text'
            className='form-control'
            placeholder='Enter your phone number'
          />
          <p className='text-danger'>{errors.phone?.message}</p>
        </div>
        <div className='form-group'>
          <select className='form-select' {...register('city')}>
            <option value=''>Select your city</option>
            <option value='dakor'>Dakor</option>
            <option value='ahmedabad'>Ahmedabad</option>
            <option value='modasa'>Modasa</option>
          </select>
          <p className='text-danger'>{errors.city?.message}</p>
        </div>
        <div className='form-group'>
          <label htmlFor='msg'>Message</label>
          <textarea
            {...register('msg')}
            className='form-control'
            rows='3'
            placeholder='Enter your message(in less than 100 characters)'
          ></textarea>
          <p className='text-danger'>{errors.msg?.message}</p>
        </div>
        <button type='submit' className='btn btn-primary'>
          Submit
        </button>
      </form>
    </div>
  );
};

export default App;
