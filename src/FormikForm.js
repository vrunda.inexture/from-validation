import React from 'react';
import FormField from './components/FormField';
import { useFormik } from 'formik';
import { validationSchema } from './validation/ValidationSchema';
import './App.css';

const initialValues = {
  name: '',
  email: '',
  password: '',
  age: '',
  gender: '',
  phone: '',
  city: '',
  message: '',
};

const FormikForm = ({ onSubmit }) => {
  //using useFormik
  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  });

  //use formik.getFieldProps for input fields
  const nameProps = formik.getFieldProps('name');
  const emailProps = formik.getFieldProps('email');
  const passwordProps = formik.getFieldProps('password');
  const ageProps = formik.getFieldProps('age');
  const phoneProps = formik.getFieldProps('phone');
  const cityProps = formik.getFieldProps('city');
  const messageProps = formik.getFieldProps('message');

  return (
    <div className='mt-5'>
      <h1> Formik Form</h1>
      <hr />
      <form onSubmit={formik.handleSubmit}>
        <div className='form-group'>
          <FormField
            label='Name'
            name='name'
            id='name'
            type='text'
            className='form-control'
            placeholder='John Doe'
            {...nameProps}
          />
          {formik.touched.name && formik.errors.name ? (
            <p className='text-danger'>{formik.errors.name}</p>
          ) : null}
        </div>
        <div className='form-group'>
          <FormField
            label='Email'
            name='email'
            id='email'
            type='text'
            className='form-control'
            placeholder='name@example.com'
            {...emailProps}
          />
          {formik.touched.email && formik.errors.email ? (
            <p className='text-danger'>{formik.errors.email}</p>
          ) : null}
        </div>
        <div className='form-group'>
          <FormField
            label='Password'
            name='password'
            id='password'
            type='text'
            className='form-control'
            placeholder='Enter your password'
            {...passwordProps}
          />
          {formik.touched.password && formik.errors.password ? (
            <p className='text-danger'>{formik.errors.password}</p>
          ) : null}
        </div>
        <div className='form-group'>
          <FormField
            label='Age'
            name='age'
            id='age'
            type='text'
            className='form-control'
            placeholder='Enter your age'
            {...ageProps}
          />
          {formik.touched.age && formik.errors.age ? (
            <p className='text-danger'>{formik.errors.age}</p>
          ) : null}
        </div>

        <div className='form-group'>
          <FormField
            label='Phone'
            name='phone'
            type='text'
            id='phone'
            className='form-control'
            placeholder='Enter your phone number'
            {...phoneProps}
          />
          {formik.touched.phone && formik.errors.phone ? (
            <p className='text-danger'>{formik.errors.phone}</p>
          ) : null}
        </div>
        <div className='form-group'>
          <select
            label='city'
            className='form-select'
            {...cityProps}
            name='city'
            id='city'
          >
            <option value=''>Select your city</option>
            <option value='dakor'>Dakor</option>
            <option value='ahmedabad'>Ahmedabad</option>
            <option value='modasa'>Modasa</option>
          </select>
          {formik.touched.city && formik.errors.city ? (
            <p className='text-danger'>{formik.errors.city}</p>
          ) : null}
        </div>
        <div className='form-group'>
          <label htmlFor='message'>Message</label>
          <textarea
            className='form-control'
            rows='3'
            name='message'
            id='message'
            placeholder='Enter your message(in less than 100 characters)'
            {...messageProps}
          ></textarea>
          {formik.touched.message && formik.errors.message ? (
            <p className='text-danger'>{formik.errors.message}</p>
          ) : null}
        </div>
        <button type='submit' className='btn btn-primary'>
          Submit
        </button>
      </form>
    </div>
  );
};

export default FormikForm;
