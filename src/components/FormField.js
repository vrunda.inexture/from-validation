import React from 'react';

function FormField({ name, label, ...rest }) {
  return (
    <div>
      <label htmlFor={name}>{label}</label>
      <input name={name} {...rest} />
    </div>
  );
}

export default FormField;
