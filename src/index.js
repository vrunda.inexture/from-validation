import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import FormikForm from './FormikForm';

ReactDOM.render(
  <React.StrictMode>
    <div className='container'>
      <div className='row'>
        <div className='col-md-6'>
          <App />
        </div>
        <div className='col-md-6'>
          <FormikForm />
        </div>
      </div>
    </div>
  </React.StrictMode>,
  document.getElementById('root')
);
