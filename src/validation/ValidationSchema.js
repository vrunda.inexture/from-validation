import * as yup from 'yup';

// validaton schema
export const validationSchema = yup.object().shape({
  name: yup
    .string()
    .matches(/^[aA-zZ\s]+$/, 'Only alphabets are allowed for this field ')
    .required('Name is a required field')
    .min(3, 'Name must be at least 3 characters'),

  email: yup.string().email().required('Email is a required field'),

  password: yup
    .string()
    .required('Please enter your password')
    .matches(
      /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
      'Password must contain at least 8 characters, one uppercase, one number and one special case character'
    ),

  age: yup
    .string()
    .required('Please supply your age')
    .matches(/[0-9]/, 'Age must be a number')
    .matches(
      /1[8-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]|6[0]/,
      'Age must be between 8 to 60'
    ),

  gender: yup.string().required(),

  phone: yup
    .string()
    .required()
    .matches(/^[0-9]+$/, 'Phone number must be only digits')
    .min(10, 'Must be exactly 10 digits')
    .max(10),

  city: yup.string().required(),

  message: yup.string().max(100, 'You can enter max 100 characters only.'),
});
